# IgH Doxygen Theme

## Doxyfile adaptions

This repo is assumed to be included as git submodule.

Set the following options in Doxyfile:
```
PROJECT_LOGO           = doxygen-layout/images/igh+logo.svg

HTML_HEADER            = doxygen-layout/style/html_header.html
HTML_FOOTER            = doxygen-layout/style/html_footer.html
HTML_EXTRA_STYLESHEET  = doxygen-layout/style/custom.css \
                         doxygen-layout/style/custom_igh_theme.css
HTML_EXTRA_FILES       = doxygen-layout/images/etherlab-128.png \
                         doxygen-layout/images/favicon.ico \
                         doxygen-layout/images/favicon.svg
 

```
